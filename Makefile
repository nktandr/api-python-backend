docker_run:
	docker-compose up

docker_build:
	docker-compose up --build

docker_app:
	docker exec -it backend_app bash

docker_make_migrations:
	docker exec -it backend_app python manage.py makemigrations

docker_migrate: docker_make_migrations
	docker exec -it backend_app python manage.py migrate

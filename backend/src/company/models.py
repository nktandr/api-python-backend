from django.db import models


class Company(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField()
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.title or 'Нет Компании'

    class Meta:
        verbose_name = 'Компанию'
        verbose_name_plural = 'Компании'

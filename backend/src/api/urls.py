from django.urls import path, include

urlpatterns = [
    # API
    path('', include('api.v2.urls')),
]
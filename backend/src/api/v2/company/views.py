from rest_framework import generics
from company.models import Company

from .serializers import ListCompaniesSerializer


class CompanyListView(generics.ListAPIView):
    queryset = Company.objects.filter(is_active=True)
    serializer_class = ListCompaniesSerializer

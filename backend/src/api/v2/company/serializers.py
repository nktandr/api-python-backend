from rest_framework import serializers

from company.models import Company


class ListCompaniesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'title']
        # fields = '__all__'

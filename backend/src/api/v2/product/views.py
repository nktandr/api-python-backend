from rest_framework import generics
from product.models import Product

from .serializers import ListProductsSerializer, CreateProductsSerializer


class ProductsListView(generics.ListAPIView):
    queryset = Product.objects.filter(is_active=True)
    serializer_class = ListProductsSerializer


class ProductDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ListProductsSerializer


class CreateProductView(generics.CreateAPIView):
    queryset = Product.objects.all()
    serializer_class = CreateProductsSerializer

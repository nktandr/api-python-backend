from rest_framework import serializers

from product.models import Product

from api.v2.category.serializers import ListCategoriesSerializer

from api.v2.company.serializers import ListCompaniesSerializer


class ListProductsSerializer(serializers.ModelSerializer):
    category = ListCategoriesSerializer(read_only=True)
    company = ListCompaniesSerializer(read_only=True)

    class Meta:
        model = Product
        fields = '__all__'


class CreateProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['title', 'description', 'category', 'company', 'is_active', ]

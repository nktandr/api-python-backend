from django.urls import path
from . import views

urlpatterns = [
    path('/add/', views.CreateProductView.as_view(), name='add_product'),
    path('/<int:pk>/', views.ProductDetailView.as_view(), name='detail_product'),
    path('', views.ProductsListView.as_view(), name='all_products'),
]

from django.urls import path, include

urlpatterns = [
    path('categories', include('api.v2.category.urls')),
    path('companies', include('api.v2.company.urls')),
    path('products', include('api.v2.product.urls')),
]

# Generated by Django 3.1.5 on 2021-01-14 13:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project_user', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='profile',
            options={'verbose_name': 'Пользователь', 'verbose_name_plural': 'Пользователи'},
        ),
    ]

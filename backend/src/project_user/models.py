from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField


class Profile(AbstractUser):
    phone = PhoneNumberField()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

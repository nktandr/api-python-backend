from django.contrib import admin

from project_user.models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass

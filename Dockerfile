FROM python:buster
ENV PYTHONUNBUFFERED=1

VOLUME ["/backend"]

RUN apt update
RUN apt install nginx npm vim -y


COPY requirements.txt /backend
WORKDIR /backend

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

#ENTRYPOINT sh entrypoint.sh
